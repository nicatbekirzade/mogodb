package com.example.mongodb;

import com.example.mongodb.model.User;
import com.example.mongodb.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class MongoDbApplication implements CommandLineRunner {

    private final MongoTemplate mongoTemplate;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(MongoDbApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setName("Mike");
        mongoTemplate.insert(user, "user");
        userRepository.save(user);
        //********************************
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is("Nijat"));

        List<User> users = mongoTemplate.find(query, User.class);
        System.out.println("From template query");
        users.forEach(System.out::println);

//        User byId = mongoTemplate.findById("62a31953e8cc540b0b17e6f2", User.class);

//        System.out.println("ById: "+ byId);

        System.out.println("from Repo");
        userRepository.findAll().forEach(System.out::println);
    }
}
